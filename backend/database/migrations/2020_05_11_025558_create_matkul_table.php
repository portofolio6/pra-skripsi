<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatkulTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matkul', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_dosen')->unsigned();
            $table->bigInteger('id_mahasiswa')->unsigned();
            $table->char('kd_matkul', 6);
            $table->string('nama_matkul', 30);
            $table->char('kelas', 6);
            $table->integer('semester');
            $table->foreign('id_dosen')->references('id')->on('dosen');
            $table->foreign('id_mahasiswa')->references('id')->on('mahasiswa');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matkul');
    }
}
