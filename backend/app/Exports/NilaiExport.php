<?php

namespace App\Exports;

use App\Matkul;
use Maatwebsite\Excel\Concerns\FromCollection;

class NilaiExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct($kode, $kelas) {
        $this->kode = $kode;
        $this->kelas = $kelas;
    }
    public function collection()
    {
        $matkul = Matkul::where('kd_matkul', $this->kode)->where('kelas', $this->kelas)->with('mahasiswa')->with('nilai')->get();
        return $matkul;
    }
}
