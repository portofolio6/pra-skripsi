import Vue from 'vue'
import VueRouter from 'vue-router'
import Cookies from 'js-cookie'
import HomeAdmin from '../views/HomeAdmin.vue'
import HomeUser from '../views/HomeUser.vue'
import Login from '../views/Login.vue'
import Register from '../views/Register.vue'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/admin/dashboard',
      name: 'HomeAdmin',
      component: HomeAdmin,
      meta: {
        auth: true
      },
    },
    {
      path: '/',
      name: 'HomeUser',
      component: HomeUser
    },
    {
      path: '/admin/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/admin/register',
      name: 'Register',
      component: Register
    },
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.auth)) {
    if (!Cookies.get('token')) {
      next({
        path: '/admin/login',
        query: { redirect: to.fullPath }
      })
    }else{
      next();
    }
  } else {
      next();
  }
});

export default router
