<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use  App\Admin;

class UserController extends Controller
{
     /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get the authenticated User.
     *
     * @return Response
     */
    public function profile()
    {
        try{
            return response()->json(['user' => Auth::user()], 200);
        }catch (\Exception $e) {

            return response()->json(['message' => 'user not found!'], 404);
        }


    }

    /**
     * Get all User.
     *
     * @return Response
     */
    public function allUsers()
    {
         return response()->json(['users' =>  Admin::all()], 200);
    }

    /**
     * Get one user.
     *
     * @return Response
     */
    public function singleUser($username)
    {
        try {
            $admin = Admin::where('username',$username)->first();

            return $admin;

        } catch (\Exception $e) {

            return response()->json(['message' => 'user not found!'], 404);
        }

    }

}
