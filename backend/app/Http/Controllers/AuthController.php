<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Admin;

class AuthController extends Controller
{
    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function register(Request $request)
    {
        //validate incoming request
        $this->validate($request, [
            'nama' => 'required|string',
            'username' => 'required|unique:admin',
            'password' => 'required|confirmed',
        ]);

        try {
            $admin = new Admin;
            $admin->nama = $request->input('nama');
            $admin->username = $request->input('username');
            $plainPassword = $request->input('password');
            $admin->password = app('hash')->make($plainPassword);

            $admin->save();

            //return successful response
            return response()->json(['admin' => $admin, 'message' => 'CREATED'], 201);

        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => 'User Registration Failed!'], 409);
        }
    }

    /**
     * Get a JWT via given credentials.
     *
     * @param  Request  $request
     * @return Response
     */
    public function login(Request $request)
    {
          //validate incoming request
        $this->validate($request, [
            'username' => 'required|string',
            'password' => 'required|string',
        ]);

        $credentials = $request->only(['username', 'password']);

        if (! $token = Auth::attempt($credentials)) {
            return response()->json(['message' => 'Akun Belum Terdaftar'], 401);
        }
        
        return $this->respondWithToken($token);
    }

}
