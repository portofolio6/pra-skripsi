<?php
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/random-key', function () {
    return Str::random(64);
});

$router->group(['prefix' => 'nilai'], function () use ($router) {
    $router->get('/get-nilai', 'NilaiController@getNilai');
    $router->get('/get-nilai/username/{input}', 'NilaiController@getNilaiByAdmin');
    $router->get('/get-rerata', 'NilaiController@getRerata');
    // $router->get('/get-nilai/kelas/{input}', 'NilaiController@filterNilaiByKelasUser');
    // $router->get('/get-nilai/admin-username/{input2}/kelas/{input}', 'NilaiController@filterNilaiByKelas');
    $router->get('/get-nilai/kode/{input}/kelas/{input2}', 'NilaiController@filterNilaiByKdMatkulUser');
    $router->get('/get-nilai/kode/{input}/kelas/{input2}/admin-username/{input3}', 'NilaiController@filterNilaiByMatkul');
    $router->get('/get-tahun', 'NilaiController@getTahun');
    $router->post('/post-nilai', 'NilaiController@postNilai');
    $router->delete('/delete-nilai/{id}', 'NilaiController@deleteNilai');
    $router->put('/put-nilai/{id}', 'NilaiController@putNilai');
});

$router->group(['prefix' => 'mahasiswa'], function () use ($router) {
    $router->get('/get-mahasiswa', 'MahasiswaController@getMahasiswa');
});

$router->group(['prefix' => 'matkul'], function () use ($router) {
    $router->get('/get-matkul', 'MatkulController@getMatkul');
    $router->get('/export-nilai/kode/{kode}/kelas/{kelas}', 'MatkulController@export');
    $router->get('/get-nilai/kode/{kode}/kelas/{kelas}', 'MatkulController@getNilaiByMatkul');
    $router->get('/get-matkul/kode/{kode}/kelas/{kelas}', 'MatkulController@getMatkulByNamaKelas');
});

$router->group(['prefix' => 'auth'], function () use ($router) {
    $router->post('register', 'AuthController@register');
    $router->post('login', 'AuthController@login');
    $router->post('logout', 'AuthController@logout');
    $router->get('profile', 'UserController@profile');
    $router->get('user/{username}', 'UserController@singleUser');
    $router->get('users', 'UserController@allUsers');
});
