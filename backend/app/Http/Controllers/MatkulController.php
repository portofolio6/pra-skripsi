<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Exports\NilaiExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Matkul;
use App\Mahasiswa;

class MatkulController extends Controller
{
    /**
     * get mahasiswa data
     *
     * @return void
     */
    public function getMatkul()
    {
        $matkul = Matkul::with('mahasiswa')->get();
        return $matkul;
    }
    public function getNilaiByMatkul($kode, $kelas)
    {
        $matkul = Matkul::where('kd_matkul', $kode)->where('kelas', $kelas)->with('mahasiswa')->with('nilai')->paginate(25);
        return $matkul;
    }

    public function getMatkulByNamaKelas($kode, $kelas){
        $matkul = Matkul::where('kd_matkul', $kode)->where('kelas', $kelas)->with('mahasiswa')->get();
        return $matkul;
    }

    public function export($kode, $kelas)
    {
        return Excel::download(new NilaiExport($kode, $kelas), 'Nilai.xlsx');
    }

}
