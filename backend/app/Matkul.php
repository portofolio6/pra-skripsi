<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Matkul extends Model
{
    protected $table = 'matkul';
    public $timestamps = 'true';

    public function matkul()
    {
        return $this->belongsTo('App\Matkul', 'id');
    }

    public function mahasiswa()
    {
        return $this->belongsTo('App\Mahasiswa', 'id_mahasiswa');
    }

    public function nilai()
    {
        return $this->hasMany('App\Nilai', 'id_matkul')->orderBy('pertemuan');

    }
}
