<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nilai extends Model
{
    protected $table = 'nilai';
    protected $guarded = [];
    public $timestamps = 'true';

    public function matkul()
    {
        return $this->belongsTo('App\Matkul', 'id_matkul');
    }
    public function admin()
    {
        return $this->belongsTo('App\Admin', 'id_admin');
    }

}
