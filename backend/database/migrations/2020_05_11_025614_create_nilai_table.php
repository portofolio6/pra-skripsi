<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNilaiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nilai', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_matkul')->unsigned();
            $table->bigInteger('id_admin')->unsigned();
            $table->integer('pertemuan');
            $table->string('semester');
            $table->string('thn_akademik');
            $table->double('pretest');
            $table->double('laporan');
            $table->double('nilai_total');
            $table->foreign('id_matkul')->references('id')->on('matkul');
            $table->foreign('id_admin')->references('id')->on('admin');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nilai');
    }
}
