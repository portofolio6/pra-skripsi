<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mahasiswa;

class MahasiswaController extends Controller
{
    /**
     * get mahasiswa data
     *
     * @return void
     */
    public function getMahasiswa()
    {
        $mahasiswa = Mahasiswa::all();
        return $mahasiswa;
    }

}
