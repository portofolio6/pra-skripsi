<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Nilai;
use App\Matkul;
use App\Mahasiswa;
use App\Dosen;
use App\Admin;

class NilaiController extends Controller
{
    public function getNilai(){
        $nilai = Nilai::with('matkul.mahasiswa')->paginate(25);
        return $nilai;
    }

    public function getNilaiByAdmin($input){
        $nilai = Nilai::with('matkul.mahasiswa')->with('admin')->get();
        $collections = collect($nilai);
        $filter = $collections->filter(function ($value, $key) use ($input){
            return $value->admin->username == $input;
        })->paginate(25);

        return $filter;
    }

    // public function filterNilaiByKelasUser($input){
    //     $nilai = Nilai::with('matkul.mahasiswa')->with('admin')->get();
    //     $collections = collect($nilai);
    //     $filter = $collections->filter(function ($value, $key) use ($input){
    //         return $value->matkul->kelas == $input;
    //     })->paginate(25);

    //     return $filter;
    // }

    // public function filterNilaiByKelas($input, $input2){
    //     $nilai = Nilai::with('matkul.mahasiswa')->with('admin')->get();
    //     $collections = collect($nilai);
    //     $filter = $collections->filter(function ($value, $key) use ($input, $input2){
    //         return $value->matkul->kelas == $input && $value->admin->username == $input2;
    //     })->paginate(25);

    //     return $filter;
    // }

    public function filterNilaiByKdMatkulUser($input, $input2){
        $nilai = Nilai::with('matkul.mahasiswa')->with('admin')->get();
        $collections = collect($nilai);
        $filter = $collections->filter(function ($value, $key) use ($input, $input2){
            return $value->matkul->kd_matkul == $input && $value->matkul->kelas == $input2;
        })->paginate(25);

        return $filter;
    }

    public function filterNilaiByMatkul($input, $input2, $input3){
        $nilai = Nilai::with('matkul.mahasiswa')->with('admin')->get();
        $collections = collect($nilai);
        $filter = $collections->filter(function ($value, $key) use ($input, $input2, $input3){
            return $value->matkul->kd_matkul == $input && $value->matkul->kelas == $input2 && $value->admin->username == $input3;
        })->paginate(25);

        return $filter;
    }

    public function postNilai(Request $request){
        $nilai = new Nilai;
        $nilai->id_matkul = $request->id_matkul;
        $nilai->id_admin = $request->id_admin;
        $nilai->semester = $request->semester;
        $nilai->thn_akademik = $request->thn_akademik;
        $nilai->pertemuan = $request->pertemuan;
        $nilai->pretest = $request->pretest;
        $nilai->laporan = $request->laporan;
        $nilai->nilai_total = ($request->laporan * 60/100) + ($request->pretest * 40/100);
        $nilai->save();

        return $nilai;
    }

    public function putNilai(Request  $request, $id){
        $nilai = Nilai::find($id);
        $nilai->id_matkul = $request->id_matkul;
        $nilai->pertemuan = $request->pertemuan;
        $nilai->semester = $request->semester;
        $nilai->thn_akademik = $request->thn_akademik;
        $nilai->pretest = $request->pretest;
        $nilai->laporan = $request->laporan;
        $nilai->nilai_total = ($request->laporan * 60/100) + ($request->pretest * 40/100);
        $nilai->save();

        return $nilai;
    }

    public function deleteNilai($id){
        $nilai = Nilai::find($id);
        $nilai->delete();
        return $nilai;
    }

    public function getRerata(){
        return Nilai::avg('nilai_total');
    }

    public function getTahun(){
        $tahunAwal = 1999;
        $tahunSekarang = date("Y");
        $hasil=[];
        while ($tahunAwal <= $tahunSekarang) {
            $hasil[] = $tahunAwal."/".$tahunAwal += 1;
            // echo $hasil;
            // echo "<br>";
        }
        return $hasil;
    }
}
